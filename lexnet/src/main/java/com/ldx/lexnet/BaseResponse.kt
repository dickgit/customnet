package com.ldx.lexnet


/**
 * response body的基类
 * 1.需要根据项目后台返回的具体数据结构决定,该结构为通用结构
 */
class BaseResponse<T> {
    val code = 0
    val data: T? = null
    val retmsg: String? = null
}