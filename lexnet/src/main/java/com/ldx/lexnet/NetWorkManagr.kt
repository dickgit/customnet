package com.ldx.lexnet

import android.content.Context
import android.util.Log
import com.google.gson.GsonBuilder
import com.ldx.lexnet.custombody.DowProgressRB
import com.ldx.lexnet.custombody.UploadProgressRB
import com.ldx.lexnet.custominterceptor.*
import com.ldx.lexnet.util.HTTPSCerUtils
import okhttp3.Call
import okhttp3.EventListener
import okhttp3.OkHttpClient
import okhttp3.Protocol
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.net.InetSocketAddress
import java.net.Proxy


/**
 * 单例的Retrofit单例管理类
 * 1.在Application初始化
 */
object NetWorkManager {

    private var retrofit: Retrofit? = null
    private var mOkHttp: OkHttpClient? = null
    private var mContext: Context? = null
    private var mBaseUrl = "" //baseUrl
    private var ifDebug = false//是否是调试模式


    private var headerCommonParamsMap: HashMap<String, Any>? = null //请求头通用参数
    private var commonParamsInterceptor: CommonParamsInterceptor? = null //通用参数拦截器
    private var loggingIntercept: HttpLoggingInterceptor? = null
    private var downloadInterceptor: DownloadInterceptor? = null
    private var uploadInterceptor: UploadInterceptor? = null
    private var timeoutInterceptor: TimeoutInterceptor? = null
    private var baseUrlInterceptor: BaseUrlInterceptor? = null

    /**
     * 设置baseUrl
     * 1.通过拦截器动态修改
     */
    fun setBaseUrl(url: String) {
        this.mBaseUrl = url
    }

    /**
     * 动态设置请求通用参数
     */
    fun setCommonParams(
        head: HashMap<String, Any>? = null,
        query: HashMap<String, Any>? = null,
        form: HashMap<String, Any>? = null
    ) {
        if (head != null) {
            commonParamsInterceptor?.addHeaderParams(head)
        }
        if (query != null) {
            commonParamsInterceptor?.addQueryParams(query)
        }
        if (form != null) {
            commonParamsInterceptor?.addFormParams(form)
        }
    }

    /**
     * 动态移除请求通用参数
     */
    fun removeCommonParams(
        ifRemoveHead: Boolean = false,
        ifRemoveQuery: Boolean = false,
        ifRemoveForm: Boolean = false
    ) {
        if (ifRemoveHead) {
            commonParamsInterceptor?.removeHeaderParams()
        }
        if (ifRemoveQuery) {
            commonParamsInterceptor?.removeQueryParams()
        }
        if (ifRemoveForm) {
            commonParamsInterceptor?.removeFormParams()
        }
    }

    /**
     * 初始化必要对象和参数
     * 1.建议在Application初始化
     * 2.设置baseUrl
     * 3.设置通用参数
     * 4.设置信任证书
     * 5.是否开启debug模式
     */
    fun init(
        context: Context,
        baseUrl: String,
        headerCommonParamsMap: HashMap<String, Any>? = HashMap(),
        cerAssetsName: String = "",
        isDebug: Boolean = false
    ) {
        this.ifDebug = isDebug
        mContext = context
        mBaseUrl = baseUrl
        NetWorkManager.headerCommonParamsMap = headerCommonParamsMap
        initOkHttp(cerAssetsName)
        initRetrofit()
    }

    /**
     * 初始化OkHttp
     */
    @Override
    private fun initOkHttp(cerAssetsName: String = "") {
        initInterceptor()
        var mOkHttpBuilder = OkHttpClient.Builder()// 初始化OkHttp配置
            .addInterceptor(baseUrlInterceptor!!)
            .addInterceptor(timeoutInterceptor!!)
            .addInterceptor(commonParamsInterceptor!!)
            .addInterceptor(uploadInterceptor!!)
            .addInterceptor(loggingIntercept!!)
        if (cerAssetsName != "") {
            mOkHttpBuilder =
                HTTPSCerUtils.setCertificateAsset(mContext, mOkHttpBuilder, cerAssetsName)
        }
        mOkHttp = mOkHttpBuilder.build()

    }

    /**
     * 初始化拦截器对象
     */
    private fun initInterceptor() {
        commonParamsInterceptor =
            CommonParamsInterceptor.Builder().addHeaderParamsMap(headerCommonParamsMap).build()
        downloadInterceptor = DownloadInterceptor()
        uploadInterceptor = UploadInterceptor()
        timeoutInterceptor = TimeoutInterceptor()
        baseUrlInterceptor = BaseUrlInterceptor()
        loggingIntercept = HttpLoggingInterceptor {
            Log.d(
                "netlog",
                "onResponse message=$it"
            )
        }
        if (ifDebug) {
            loggingIntercept?.level = HttpLoggingInterceptor.Level.BODY
        } else {
            loggingIntercept?.level = HttpLoggingInterceptor.Level.BASIC
        }
    }

    /**
     * 初始化Retrofit
     */
    private fun initRetrofit() {
        val gSon = GsonBuilder().registerTypeAdapterFactory(GsonTypeAdapterFactory()).create()
        retrofit = Retrofit.Builder()// 初始化Retrofit
            .client(mOkHttp!!)
            .baseUrl(mBaseUrl)//baseUrl
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())//配合RXJava使用
            .addConverterFactory(GsonConverterFactory.create(gSon))//使用GSON解析库
            .build()

    }


    /**
     * 创建Http请求
     * 1.上传回调监听
     * 2.下载回调监听
     * 3.替换baseUrl
     * 4.http超时时间配置
     */
    fun <T> create(
        clazz: Class<T>,
        upListener: UploadProgressRB.UploadListener? = null,
        downListener: DowProgressRB.DownLoadProgressListener? = null,
        replaceBaseUrl: String = mBaseUrl,
        cTimeout: Long? = null, wTimeout: Long? = null, rTimeout: Long? = null
    ): T? {
        uploadInterceptor?.upLoadProgressListener = upListener
        downloadInterceptor?.downLoadProgressListener = downListener
        baseUrlInterceptor?.replaceUrl = replaceBaseUrl
        timeoutInterceptor?.setTimeOut(cTimeout, wTimeout, rTimeout)
        return if (retrofit != null) {
            retrofit?.create(clazz)
        } else null
    }


    /**
     * Http请求过程监听回调,可用来统计http请求时间
     */
    private val eventListener = object : EventListener() {
        override fun connectStart(call: Call, inetSocketAddress: InetSocketAddress, proxy: Proxy) {
            super.connectStart(call, inetSocketAddress, proxy)
        }

        override fun connectEnd(
            call: Call,
            inetSocketAddress: InetSocketAddress,
            proxy: Proxy,
            protocol: Protocol?
        ) {
            super.connectEnd(call, inetSocketAddress, proxy, protocol)
        }
    }


}