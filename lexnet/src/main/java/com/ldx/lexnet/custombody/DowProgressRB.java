package com.ldx.lexnet.custombody;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.ResponseBody;
import okio.Buffer;
import okio.BufferedSource;
import okio.ForwardingSource;
import okio.Okio;
import okio.Source;

/**
 *
 * 自定义ResponseBody
 * 1.监听下载进度
 */
public class DowProgressRB extends ResponseBody{
    private final ResponseBody responseBody;
    private final DownLoadProgressListener downLoadProgressListener;
    private BufferedSource bufferedSource;
    private final String url;

    public interface DownLoadProgressListener {
        void update(String url, long bytesRead, long contentLength, boolean done);
    }

    DowProgressRB(String url, ResponseBody responseBody, DownLoadProgressListener progressListener) {
        this.responseBody = responseBody;
        this.downLoadProgressListener = progressListener;
        this.url = url;
    }

    @Override
    public MediaType contentType() {
        return responseBody.contentType();
    }

    @Override
    public long contentLength() {
        return responseBody.contentLength();
    }

    @NotNull
    @Override
    public BufferedSource source() {
        if (bufferedSource == null) {
            bufferedSource = Okio.buffer(source(responseBody.source()));
        }
        return bufferedSource;
    }

    private Source source(final Source source) {
        return new ForwardingSource(source) {
            long totalBytesRead = 0L;

            @Override
            public long read(@NotNull Buffer sink, long byteCount) throws IOException {
                long bytesRead = super.read(sink, byteCount);
                totalBytesRead += bytesRead != -1 ? bytesRead : 0;
                downLoadProgressListener.update(url, totalBytesRead, responseBody.contentLength(), bytesRead == -1);
                return bytesRead;
            }
        };
    }

}
