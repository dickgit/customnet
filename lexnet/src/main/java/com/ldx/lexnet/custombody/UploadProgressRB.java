package com.ldx.lexnet.custombody;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okio.Buffer;
import okio.BufferedSink;
import okio.ForwardingSink;
import okio.Okio;
import okio.Sink;

/**
 * 自定义RequestBody
 * 1.监听文件上传进度
 */

public class UploadProgressRB extends RequestBody {
    private final RequestBody mRequestBody;
    private final UploadListener mUploadListener;

    public interface UploadListener {
        void onRequestProgress(long bytesWritten, long contentLength);
    }

    public UploadProgressRB(RequestBody requestBody, UploadListener uploadListener) {
        mRequestBody = requestBody;
        mUploadListener = uploadListener;
    }

    @Override
    public MediaType contentType() {
        return mRequestBody.contentType();
    }

    @Override
    public long contentLength() throws IOException {
        try {
            return mRequestBody.contentLength();
        } catch (IOException e) {
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public void writeTo(@NotNull BufferedSink sink) throws IOException {
        BufferedSink bufferedSink;
        CountingSink mCountingSink = new CountingSink(sink);
        bufferedSink = Okio.buffer(mCountingSink);
        mRequestBody.writeTo(bufferedSink);
        bufferedSink.flush();
    }

    class CountingSink extends ForwardingSink {

        private long bytesWritten = 0;
        private long contentLength = 0;
        public CountingSink(Sink delegate) {
            super(delegate);
        }

        @Override
        public void write(@NotNull Buffer source, long byteCount) throws IOException {
            super.write(source, byteCount);
            if (contentLength == 0) {
                contentLength = contentLength();
            }
            bytesWritten += byteCount;
            mUploadListener.onRequestProgress(bytesWritten, contentLength);
        }
    }
}
