package com.ldx.lexnet.custominterceptor

import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.Response

/**
 * @author: dongxunliu
 * @date: 2021/2/4
 * baseurl拦截器,用于动态修改baseurl
 */
class BaseUrlInterceptor : Interceptor {
    var replaceUrl: String = ""
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val oldHttpUrl = request.url()
        val builder = request.newBuilder()
        if (replaceUrl != "") {
            val newBaseUrl = HttpUrl.parse(replaceUrl)
            val newFullUrl = oldHttpUrl// 重建新的HttpUrl，修改需要修改的url部分
                .newBuilder() // 更换网络协议
                .scheme(newBaseUrl!!.scheme()) // 更换主机名
                .host(newBaseUrl.host()) // 更换端口
                .port(newBaseUrl.port())
                .build()
            return chain.proceed(builder.url(newFullUrl).build())
        } else {
            return chain.proceed(request)
        }
    }
}