package com.ldx.lexnet.custominterceptor

import okhttp3.*
import okio.Buffer
import java.io.IOException


/**
 * @author: dongxunliu
 * @date: 2021/2/2
 * 自定义通用请求参数拦截器
 *
 */
class CommonParamsInterceptor(
    builder: Builder
) : Interceptor {
    private var headerParamsMap = HashMap<String, Any>() //请求头部分的通用参数map集合
    private var queryParamsMap = HashMap<String, Any>()//url部分的通用参数map集合
    private var formParamsMap = HashMap<String, Any>()//请求体为formdata部分的的通用参数map集合

    init {
        headerParamsMap = builder.headerParamsMap
        queryParamsMap = builder.queryParamsMap
        formParamsMap = builder.formParamsMap
    }

    /**
     * 添加请求头通用参数
     */
    fun addHeaderParams(map: HashMap<String, Any>) {
        headerParamsMap.putAll(map)
    }

    /**
     * 移除请求头通用参数
     */
    fun removeHeaderParams() {
        headerParamsMap.clear()
    }

    /**
     * 添加url部分通用参数
     */
    fun addQueryParams(map: HashMap<String, Any>) {
        queryParamsMap.putAll(map)
    }

    /**
     * 移除url部分通用参数
     */
    fun removeQueryParams() {
        queryParamsMap.clear()
    }

    /**
     * 添加请求头通用参数
     */
    fun addFormParams(map: HashMap<String, Any>) {
        formParamsMap.putAll(map)
    }

    /**
     * 移除请求头通用参数
     */
    fun removeFormParams() {
        formParamsMap.clear()
    }


    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val requestBuilder = request.newBuilder()
        val headerBuilder = request.headers().newBuilder()
        if (headerParamsMap.size > 0) {//拼接请求头通用参数
            val iterator = headerParamsMap.entries.iterator()
            while (iterator.hasNext()) {
                val entry = iterator.next() as Map.Entry<String, Any>
                headerBuilder.add(entry.key, entry.value.toString())
            }
        }
        requestBuilder.headers(headerBuilder.build())

        if (queryParamsMap.size > 0) {//拼接url通用参数
            val httpUrlBuilder = request.url().newBuilder()
            val iterator = queryParamsMap.entries.iterator()
            while (iterator.hasNext()) {
                val entry = iterator.next() as Map.Entry<String, Any>
                httpUrlBuilder.addQueryParameter(entry.key, entry.value.toString())
            }
            requestBuilder.url(httpUrlBuilder.build())
        }

        if (request.method() == "POST" && request.body()!!.contentType()!!
                .subtype() == "x-www-form-urlencoded"
        ) {//请求请求体类型为formdata格式通用参数
            val formBodyBuilder = FormBody.Builder()
            if (formParamsMap.size > 0) {
                val iterator = formParamsMap.entries.iterator()
                while (iterator.hasNext()) {
                    val entry = iterator.next() as Map.Entry<String, Any>
                    formBodyBuilder.add(entry.key, entry.value.toString())
                }
            }
            val formBody = formBodyBuilder.build()
            var postBodyString = bodyToString(request.body())
            postBodyString += (if (postBodyString.isNotEmpty()) "&" else "") + bodyToString(formBody)
            requestBuilder.post(
                RequestBody.create(
                    MediaType.parse("application/x-www-form-urlencoded;charset=UTF-8"),
                    postBodyString
                )
            )
        }
        return chain.proceed(requestBuilder.build())
    }

    /**
     * byte to String
     */
    private fun bodyToString(request: RequestBody?): String {
        return try {
            val buffer = Buffer()
            if (request != null) request.writeTo(buffer) else return ""
            buffer.readUtf8()
        } catch (e: IOException) {
            "did not work"
        }
    }

    class Builder {
        var headerParamsMap = HashMap<String, Any>() //请求头部分的通用参数map集合
        var queryParamsMap = HashMap<String, Any>()//url部分的通用参数map集合
        var formParamsMap = HashMap<String, Any>()//请求体为formdata部分的的通用参数map集合

        fun addHeaderParamsMap(map: HashMap<String, Any>?): Builder {
            if (map != null) {
                headerParamsMap = map
            }
            return this
        }

        fun addQueryParamsMap(map: HashMap<String, Any>?): Builder {

            if (map != null) {
                queryParamsMap = map
            }
            return this
        }

        fun addBodyParamsMap(map: HashMap<String, Any>?): Builder {
            if (map != null) {
                formParamsMap = map
            }
            return this
        }

        fun build(): CommonParamsInterceptor {
            return CommonParamsInterceptor(this)
        }
    }

}