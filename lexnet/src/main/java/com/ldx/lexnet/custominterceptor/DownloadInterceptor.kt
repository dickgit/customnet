package com.ldx.lexnet.custominterceptor

import com.ldx.lexnet.custombody.DowProgressRB


import okhttp3.*

/**
 * @author: dongxunliu
 * @date: 2021/2/2
 *
 */
class DownloadInterceptor : Interceptor {

    var downLoadProgressListener: DowProgressRB.DownLoadProgressListener? = null //下载监听回调


    override fun intercept(chain: Interceptor.Chain): Response {
        val originalResponse = chain.proceed(chain.request())
        if (downLoadProgressListener != null) {
//            return originalResponse.newBuilder()
//                .body(
//                    DowProgressRB(
//                        chain.request().url().url().toString(),
//                        originalResponse.body(),
//                        downLoadProgressListener
//                    )
//                )
//                .build()
            return chain.proceed(chain.request())
        } else {
            return chain.proceed(chain.request())
        }

    }


}