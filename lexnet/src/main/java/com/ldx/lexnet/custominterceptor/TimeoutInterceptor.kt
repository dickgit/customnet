package com.ldx.lexnet.custominterceptor

import okhttp3.*
import java.util.concurrent.TimeUnit


/**
 * @author: dongxunliu
 * @date: 2021/2/2
 * http超时时间拦截器
 *1.使用前需设置需要修改超时时间的接口
 */
class TimeoutInterceptor : Interceptor {
    private val TIMEOUT_NORMAL = 5L //普通接口默认超时时间,单位秒
    private var connectTimeout = TIMEOUT_NORMAL
    private var writeTimeout = TIMEOUT_NORMAL
    private var readTimeout = TIMEOUT_NORMAL

    fun setTimeOut(cTimeout: Long? = TIMEOUT_NORMAL, wTimeout: Long? = TIMEOUT_NORMAL, rTimeout: Long? = TIMEOUT_NORMAL) {
        connectTimeout = cTimeout ?: TIMEOUT_NORMAL
        writeTimeout = wTimeout ?: TIMEOUT_NORMAL
        readTimeout = rTimeout ?: TIMEOUT_NORMAL
    }


    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val questUrl = request.url().toString()
        return chain.withConnectTimeout(
            connectTimeout.toInt(),
            TimeUnit.SECONDS
        ).withReadTimeout(readTimeout.toInt(), TimeUnit.SECONDS)
            .withWriteTimeout(writeTimeout.toInt(), TimeUnit.SECONDS)
            .proceed(request)
    }


}