package com.ldx.lexnet.custominterceptor

import com.ldx.lexnet.custombody.UploadProgressRB
import okhttp3.*

/**
 * @author: dongxunliu
 * @date: 2021/2/2
 *
 */
class UploadInterceptor : Interceptor {

    var upLoadProgressListener: UploadProgressRB.UploadListener? = null //上传监听回调


    override fun intercept(chain: Interceptor.Chain): Response {
        val requestNew = chain.request()
        if (upLoadProgressListener != null) {
            val build = requestNew.newBuilder()
                .method(
                    requestNew.method(),
                    UploadProgressRB(
                        requestNew.body(),
                        upLoadProgressListener
                    )
                )
                .build()
            return chain.proceed(build)
        } else {
            return chain.proceed(chain.request())
        }
    }

}