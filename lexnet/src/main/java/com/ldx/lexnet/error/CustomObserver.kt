package com.ldx.lexnet.error

import com.module.ldxnet.error.INetCallBack
import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.disposables.Disposable
import retrofit2.HttpException

/**
 * Http请求回调监听抽象类类
 * 1.统一处理拦截后台返回的code
 * 1.只需要提供拦截的抽象类实现和一些统一的错误异常处理,其他的外部实现即可
 */
abstract class CustomObserver<T> : Observer<T>, INetCallBack<T> {

    override fun onComplete() {

    }

    override fun onSubscribe(d: Disposable) {

    }

    /**
     *Http请求过程遇到的异常
     */
    override fun onError(e: Throwable) {
        ExceptionHandler.handleHttpError(e)
        if (e is HttpException) {//Http异常
            onHttpError(e.code())
        }
    }
}