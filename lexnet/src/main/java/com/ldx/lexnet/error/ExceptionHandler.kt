package com.ldx.lexnet.error

import android.net.ParseException
import android.util.Log
import com.google.gson.Gson
import com.google.gson.JsonParseException
import com.ldx.ldxutils.CustomToastUtil
import org.json.JSONException
import retrofit2.HttpException
import java.net.ConnectException
import java.net.UnknownHostException

/**
 * http请求异常处理类
 */
object ExceptionHandler {
    /**
     * 处理Http异常
     */
    fun handleHttpError(throwable: Throwable, ifHttpException: Boolean = true) {
        Log.d("test111", "throwable=${throwable}")
        if (throwable is HttpException) {//Http异常
            if (ifHttpException) {
                CustomToastUtil.showST("HttpException errorCode= ${throwable.code()}")
            }
        } else if (throwable is ConnectException || throwable is UnknownHostException) {///连接错误
            CustomToastUtil.showST("网络连接错误")
        } else if (throwable is InterruptedException) {///连接超时
            CustomToastUtil.showST("网络连接超时")
        } else if (throwable is JsonParseException || throwable is JSONException || throwable is ParseException) {///解析错误
            CustomToastUtil.showST("数据解析错误 throwable==${throwable.message}")
        } else {//其他未知错误
            CustomToastUtil.showST("网络未知异常 throwable=${throwable.message}")
        }
    }

}
