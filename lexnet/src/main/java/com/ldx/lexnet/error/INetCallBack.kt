package com.module.ldxnet.error

/**
 * http请求常见处理
 */
interface INetCallBack<T> {
    /**
     * 请求成功(符合下列两个条件,一般用来处理标准请求成功)
     * 1.Http code 200
     * 2.和后台协定的返回成功回调
     */
    fun onSuccess(t: T)

    /**
     * token过期
     */
    fun onTokenInvalid(string: String)

    /**
     *请求成功(符合下列一个条件,一般用来处理非标准请求成功)
     * 1.Http code 200
     */
    fun onResult(t: T)


    /**
     *http非200错误码回调
     * 1.onNext只在code==200时候回调,其他会走onError
     */
    fun onHttpError(code: Int)
}