package com.ldx.lexnet.rx

import com.ldx.ldxview.DelayAlertDialog
import io.reactivex.rxjava3.core.ObservableTransformer

/**
 * @author: dongxunliu
 * @date: 2021/2/4
 */
object RxLoadingHelper {
    fun <T> loadingTransformer(dialog: DelayAlertDialog): ObservableTransformer<T, T>? {
        return ObservableTransformer { upstream ->
            upstream.doOnSubscribe {//请求开始
                if (!dialog.isShowing) {
                    dialog.showDialog()
                }
            }.doOnTerminate {//请求中断
                dialog.hideDialog()
            }.doOnComplete {//请求完成
                dialog.hideDialog()
            }.doOnError {
                dialog.hideDialog()
            }
        }
    }
}