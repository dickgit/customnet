package com.ldx.lexnet.rx


import com.ldx.ldxview.DelayAlertDialog
import com.trello.rxlifecycle4.LifecycleProvider
import com.trello.rxlifecycle4.android.FragmentEvent
import com.trello.rxlifecycle4.components.support.RxAppCompatActivity
import com.trello.rxlifecycle4.components.support.RxFragment
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.FlowableTransformer
import io.reactivex.rxjava3.core.MaybeTransformer
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableTransformer
import io.reactivex.rxjava3.schedulers.Schedulers


/**
 * RXJava的链式调用封装
 * 1.去除冗余代码
 */
object RxTransformerHelper {

    /**
     * Observable 拦截链式调用执行需求代码
     *
     */
    fun <T> observableToMain(
        rxAppCompatActivity: RxAppCompatActivity? = null,
        rxFragment: RxFragment? = null,
        dialog: DelayAlertDialog? = null
    ): ObservableTransformer<T, T> {
        return ObservableTransformer { upstream ->
            var newStream: Observable<T>? = upstream.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
            if (dialog != null) {
                newStream = newStream?.compose(RxLoadingHelper.loadingTransformer(dialog))
            }
            if (rxAppCompatActivity != null) {
                newStream = newStream?.compose(rxAppCompatActivity.bindToLifecycle())
            }
            if (rxFragment != null) {
                newStream = newStream?.compose(rxFragment.bindToLifecycle())
            }
            return@ObservableTransformer newStream
        }
    }


    /**
     * Flowable 切换到主线程
     */
    fun <T> flowableToMain(): FlowableTransformer<T, T> {
        return FlowableTransformer { upstream ->
            upstream.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
        }
    }

    /**
     * Maybe 切换到主线程
     */
    fun <T> maybeToMain(): MaybeTransformer<T, T> {
        return MaybeTransformer { upstream ->
            upstream.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
        }
    }
}
